# SHORTURL

[SHORTURL](https://evening-eyrie-98236.herokuapp.com)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Tech

* [Rails] - 4.11.2
* [RUBY] - 2.5.7
* [Postgresql] - Markdown parser done right. Fast and easy to extend.

### Installation

Install the dependencies and devDependencies and start the server.

```sh
For First time running
$ cd bitly
$ gem install bundler
$ bundle install
$ rake db:create
$ rake db:migrate
$ rails s
```

For Heroku
```sh
Create application in heroku
Login into the shell using heroku login
by using command git push heroku master we can deploy our application
```
Running Test Cases
```sh
rspec spec
```

### Plugins
| Plugin | Description |
| ------ | ------ |
| Rspec | For Testing |
| Geocoder | Get Location from Ip address |
| Capabara | Testing features |

### Developed features
1. Take the url from user in url [SHORTURL](https://evening-eyrie-98236.herokuapp.com)
2. Validating Url
3. Creating a short url for the provided Url
4. Created a link for showing analytics [SHORTURL](https://evening-eyrie-98236.herokuapp.com/stats)
5. Creating the visit link to check the no of visits
6. Expiring the url after 1 month
7. Displaying 404 page if expired or not found
