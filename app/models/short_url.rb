class ShortUrl < ActiveRecord::Base
    require 'open-uri'

    SHORT_CODE_LENGTH = 5
    validates :original_url,presence: true
    validate :check_original_url
    before_create :generate_short_url
    has_many :visits

    ## Validation for checking the original url(if it can be opened) ##
    def check_original_url
        begin
            open_status_of_url = open(original_url).status
        rescue
            errors.add(:original_url, 'Please select the valid full url(Include http | https)')
        end    
    end

    def increment_visit
        self.view_count += 1
        self.save
    end

    ## Generate Short code ##
    def generate_short_url
        url_code = ([*('a'..'z'),*('0'..'9')]).sample(SHORT_CODE_LENGTH).join
        old_codes = ShortUrl.where(short_code: url_code).last
        if old_codes.present?
            self.generate_short_url
        else
            self.short_code = url_code
        end 
    end
end

