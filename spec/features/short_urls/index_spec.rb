require 'rails_helper'

RSpec.feature "Short Url", :type => :feature do
    scenario "Create a Short Url" do
        visit "/"

        fill_in "Enter the url", :with => "https://google.com"

        click_button "Create Short url"
        
        expect(page).to have_text("http://www.example.com")
    end
end
