class CreateShortUrls < ActiveRecord::Migration
  def change
    create_table :short_urls do |t|
      t.string :original_url
      t.string :short_code
      t.string :sanitize_url
      t.integer :view_count,default: 0

      t.timestamps null: false
    end
  end
end
