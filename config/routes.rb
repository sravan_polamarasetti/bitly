Rails.application.routes.draw do
  root 'short_urls#index'
  get 'visit/:short_code' => 'short_urls#visit'
  get 'stats' => 'short_urls#stats'
  resources :short_urls,only: ['create']
end
